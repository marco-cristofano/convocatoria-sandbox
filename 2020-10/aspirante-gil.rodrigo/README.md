# Rodrigo Sebastián Gil

## Resumen
Estudiante en la [Facultad de Informática](https://www.info.unlp.edu.ar/),
de la [U.N.L.P](http://unlp.edu.ar/) de la carrera de Analista Programador
Universitario. Trabajo en el Ministerio de la Producción de la Provincia de
Buenos Aires y en el Ministerio de Producción de la Nación en mantenimiento
y desarrollo de sistemas y en la gestión de infraestructura en ambientes de
testing y producción. También fuí docente de Python para el primer año en la
Universidad de Ezeiza.  

Actualmente para la gestión de ambientes utilizamos docker y rancher 1.6, pero
me estoy capacitando en kubernetes para poder hacer el salto en algún momento.

## Antecedentes

[Aquí](https://www.linkedin.com/in/rodrigo-gil-lopetegui/)
[y aquí](https://registry.jsonresume.org/grodrigo) puede verse mi CV

![Rodrigo](./yo.jpeg)
